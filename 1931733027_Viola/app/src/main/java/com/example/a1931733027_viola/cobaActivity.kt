package com.example.a1931733027_viola

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_coba.*

class cobaActivity : AppCompatActivity() {

    var a: String = ""
    var b: String = ""
    var c: String = ""
    var d: String = ""
    var e: String = ""
    var resum: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coba)


        rg1.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb1 -> b = "lulusan SMA"
                R.id.rb2 -> b = "lulusan SMK"
                R.id.rb3 -> b = "lulusan MAN"
            }
        }
        btn1.setOnClickListener {

            a = edt1.text.toString()
            if (cb1.isChecked) c = "aktif HIMATIKA" else c = ""
            if (cb2.isChecked) d = "aktif BEM" else d = ""
            if (cb3.isChecked) e = "aktif MAPALA" else e = ""

            resum = "$a $b $c $d $e"
            tx1.setText(resum)
        }
    }
}