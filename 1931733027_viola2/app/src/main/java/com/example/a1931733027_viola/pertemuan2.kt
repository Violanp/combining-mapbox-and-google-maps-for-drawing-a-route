package com.example.a1931733027_viola

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_pertemuan2.*

class pertemuan2 : AppCompatActivity() {

    val arrayNama = arrayOf("budi margo", "agung eko", "putri satu", "putri dua")
    val arrayHobi = arrayOf("berenang","berjalan", "berlari", "makan", "tidur")

    lateinit var adapterActv : ArrayAdapter<String>
    lateinit var adapterSp1 : ArrayAdapter<String>
    lateinit var adapterLv1 : ArrayAdapter<String>
    lateinit var arrGabungan : ArrayList<String>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pertemuan2)

        adapterActv = ArrayAdapter(this, android.R.layout.simple_list_item_1,arrayNama)
        adapterSp1 = ArrayAdapter (this, android.R.layout.simple_list_item_1,arrayHobi)
        arrGabungan = ArrayList()
        adapterLv1 = ArrayAdapter(this, android.R.layout.simple_list_item_1,arrGabungan)

        actv.setAdapter(adapterActv)
        sp1.adapter = adapterSp1
        lv1.adapter = adapterLv1

        actv.threshold = 2
        actv.setOnItemClickListener { parent, view, position, id ->
            val isi = adapterActv.getItem(position)
            Toast.makeText(this, isi, Toast.LENGTH_SHORT).show()
        }
        sp1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext, "tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Toast.makeText(baseContext, adapterSp1.getItem(position), Toast.LENGTH_SHORT).show()
            }
        }
        lv1.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(baseContext, adapterSp1.getItem(position), Toast.LENGTH_SHORT).show()
        }

        btn1.setOnClickListener {
            val var1 = actv.text
            val var2 = adapterSp1.getItem(sp1.selectedItemPosition)
            val var3 = "$var1 hobinya $var2"
            arrGabungan.add(var3)
            Toast.makeText(this, var3, Toast.LENGTH_SHORT).show()
            adapterLv1.notifyDataSetChanged()
        }

    }
}
