package com.example.a1931733027_viola

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.DatePicker
import android.widget.TimePicker
import kotlinx.android.synthetic.main.activity_pertemuan3.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class pertemuan3 : AppCompatActivity(), View.OnClickListener {

    var tahun = 0
    var bulan = 0
    var hari = 0
    var jam = 0
    var menit = 0

    override fun onClick(v: View) {
        when(v?.id){
            R.id.btnDp1 ->{
                if (datePicker.visibility == View.GONE){
                    datePicker.visibility = View.VISIBLE
                    tp.visibility = View.GONE
                } else {
                    datePicker.visibility = View.GONE
                }
            }
            R.id.btnTp1 ->{
                if(tp.visibility == View.GONE) {
                    tp.visibility = View.VISIBLE
                    datePicker.visibility = View.GONE
                } else {
                    tp.visibility = View.GONE
                }
            }
            R.id.btnDpDialog -> showDialog(10)
            R.id.btnTpDialog -> showDialog(20)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pertemuan3)

        val cal: Calendar = Calendar.getInstance()

        tahun = cal.get(Calendar.YEAR)
        bulan = cal.get(Calendar.MONTH)
        hari = cal.get(Calendar.DAY_OF_MONTH)
        jam = cal.get(Calendar.HOUR)
        menit = cal.get(Calendar.MINUTE)

        txDateTime.text = "Hari ini tanggal $hari, $bulan, $tahun, jam $jam:$menit"

        datePicker.init(tahun, bulan, hari, ubahTanggal)
        btnDp1.setOnClickListener(this)
        btnTpDialog.setOnClickListener(this)

        tp.setIs24HourView(true)
        tp.setOnTimeChangedListener(ubahJam)
        btnTp1.setOnClickListener(this)
        btnDpDialog.setOnClickListener(this)
    }
        var ubahJam = TimePicker.OnTimeChangedListener { view, hourOfDay, minute ->
            txDateTime.text = "Hari ini tanggal $hari, $bulan, $tahun, jam $hourOfDay:$minute"
            jam = hourOfDay
            menit = minute
        }

        var ubahTanggal = DatePicker.OnDateChangedListener { view, year, monthOfYear, dayOfMonth ->
        txDateTime.text = "Hari ini tanggal $dayOfMonth ${monthOfYear+1}, $year, jam $jam:$menit"

        tahun = year
        bulan = monthOfYear+1
        hari = dayOfMonth
    }

        var ubahTanggalDialog = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            txDateTime.text = "Hari ini tanggal $dayOfMonth, ${month+1}, $year, jam $jam:$menit"
            tahun = year
            bulan = month+1
            hari = dayOfMonth
        }

        var ubahJamDialog = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            txDateTime.text = "Hari ini tanggal $hari, $bulan, $tahun, jam $hourOfDay:$minute"
            jam = hourOfDay
            menit = minute
        }

    override fun onCreateDialog(id: Int): Dialog {
        when(id){
            10 -> return DatePickerDialog(this, ubahTanggalDialog, tahun, bulan, hari)
            20 -> return TimePickerDialog(this, ubahJamDialog, jam, menit, true)
        }
        return super.onCreateDialog(id)
    }
}
