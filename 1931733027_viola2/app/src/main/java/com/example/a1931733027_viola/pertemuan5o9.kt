package com.example.a1931733027_viola

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_pertemuan5o9.*

class pertemuan5o9 : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        var popMenu = PopupMenu (this,v)
        popMenu.menuInflater.inflate(R.menu.menu_popup, popMenu.menu)
        popMenu.setOnMenuItemClickListener { item ->
            when(item.itemId){
                R.id.itemDownload ->{
                    var webUri = "https://www.google.com/"
                    var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(webUri))
                    startActivity(intentInternet)
                    Toast.makeText(baseContext, "masuk ke halaman",Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.itemUpload ->{
                    val intent = Intent(this, pert5interna::class.java)
                    startActivity(intent)
                    Toast.makeText(baseContext, "masuk ke halaman",Toast.LENGTH_SHORT).show()
                }

            }
            false
        }
        popMenu.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pertemuan5o9)

        registerForContextMenu(txContextMenu)
        btnPopup.setOnClickListener(this)
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        var mnuInflater = getMenuInflater()
        mnuInflater.inflate(R.menu.menu_context, menu)

    }

    override fun onContextItemSelected(item: MenuItem): Boolean {

        when (item?.itemId) {
            R.id.ItemPaste -> {
                Toast.makeText(this, "Action Paste dipilih", Toast.LENGTH_SHORT).show()
                return true
            }
            R.id.ItemDelete -> {
                var snackbar =
                    Snackbar.make(constraintLayout, "Action Delete dipilih", Snackbar.LENGTH_SHORT)
                snackbar.show()
                return true
            }
            R.id.ItemCopy -> {
                var alertBuilder = AlertDialog.Builder(this)
                alertBuilder.setTitle("Informasi").setMessage("Action Copy dipilih")
                alertBuilder.setNeutralButton("OK", null)
                alertBuilder.show()
                return true
            }
            else -> {
                Toast.makeText(this, "Tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
                return false
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemPlay ->{
                Toast.makeText(this, "Playing Music",Toast.LENGTH_SHORT).show()
                return true
            }
            R.id.itemPause-> {
                var snackBar = Snackbar.make(constraintLayout,"Pause the music",Snackbar.LENGTH_SHORT)
                snackBar.show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
