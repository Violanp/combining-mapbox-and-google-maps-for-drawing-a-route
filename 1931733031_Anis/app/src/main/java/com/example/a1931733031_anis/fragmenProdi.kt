package com.example.a1931733031_anis

import android.app.AlertDialog
import android.app.Dialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.frag_prodi.*
import kotlinx.android.synthetic.main.frag_prodi.view.*
import kotlinx.android.synthetic.main.frag_prodi.view.btnTambah
import java.lang.StringBuilder

class fragmenProdi : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnTambah -> {
                builder.setTitle("Konfirmasi").setMessage("Apakah sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnTambahDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnUbah ->{
                builder.setTitle("Konfirmasi").setMessage("Apakah benar diubah?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnUbahDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnHapus ->{
                builder.setTitle("Konfirmasi").setMessage("Apakah benar dihapus?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnHapusDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
        }
    }

    lateinit var thisParent: pertemuanenam
    lateinit var db: SQLiteDatabase
    lateinit var adapter: ListAdapter
    lateinit var v: View
    lateinit var builder: AlertDialog.Builder
    var idPro: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as pertemuanenam
        db = thisParent.getDBObject()

        v = inflater.inflate(R.layout.frag_prodi, container, false)
        v.btnTambah.setOnClickListener(this)
        v.btnUbah.setOnClickListener(this)
        v.btnHapus.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lvProdi.setOnItemClickListener(itemClick)
        return v
    }

    fun showDataProdi() {
        val cursor: Cursor = db.query(
            "prodi", arrayOf("nama_prodi", "id_prodi as _id"), null,
            null, null, null, "nama_prodi asc"
        )
        adapter = SimpleCursorAdapter(
            thisParent, R.layout.item_prodi, cursor, arrayOf("_id", "nama_prodi"),
            intArrayOf(R.id.txvId, R.id.txvProdi), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lvProdi.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataProdi()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val cursor : Cursor = parent.adapter.getItem(position) as Cursor
        idPro = cursor.getString(cursor.getColumnIndex("_id"))
        v.edtProdi.setText(cursor.getString(cursor.getColumnIndex("nama_prodi")))
    }

    fun tambahProdi(nama_prodi: String) {
        var cv: ContentValues = ContentValues()
        cv.put("nama_prodi", nama_prodi)
        db.insert("prodi", null, cv)
        showDataProdi()
    }

    val btnTambahDialog = DialogInterface.OnClickListener { dialog, which ->
        tambahProdi(v.edtProdi.text.toString())
        v.edtProdi.setText("")
    }

    fun ubahProdi(nama_prodi: String, idProdi: String) {
        var cv = ContentValues()
        cv.put("nama_prodi", nama_prodi)
        db.update("prodi", cv, "id_prodi=$idProdi", null)
        showDataProdi()
    }

    val btnUbahDialog = DialogInterface.OnClickListener { dialog, which ->
        ubahProdi(v.edtProdi.text.toString(), idPro)
        v.edtProdi.setText("")
    }

    fun hapusDialog(idProdi: String){
        db.delete("prodi", "id_prodi=$idProdi", null)
        showDataProdi()
    }
    val btnHapusDialog = DialogInterface.OnClickListener{ dialog, which ->
        hapusDialog(idPro)
        v.edtProdi.setText("")
    }

}