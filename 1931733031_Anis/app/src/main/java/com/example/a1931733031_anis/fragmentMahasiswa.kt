package com.example.a1931733031_anis

import android.app.AlertDialog
import android.app.Dialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.frag_mahasiswa.*
import kotlinx.android.synthetic.main.frag_mahasiswa.view.*
import kotlinx.android.synthetic.main.frag_mahasiswa.view.spinner
import kotlinx.android.synthetic.main.frag_prodi.*

class fragmentMahasiswa : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahMhs ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah yang Anda tambahkan sudah benar?")
                    .setPositiveButton("Ya",btnTambahMhsDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnCari -> {
                tampilDataMhs(edtNamaMhs.text.toString())
            }

            R.id.btnUbahMhs->{
                dialog.setTitle("Konfirmasi").setMessage("Yakin anda mengubah data ini ?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUbahDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnHapusMhs->{
                dialog.setTitle("Konfirmasi").setMessage("Yakin anda menghapus data ini ?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
        }
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val cursor = spAdapter.getItem(position) as Cursor
        namaProdi = cursor.getString(cursor.getColumnIndex("_id"))
    }

    lateinit var thisParent : pertemuanenam
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v : View
    lateinit var db : SQLiteDatabase
    var namaProdi : String = ""
    var namaMhs : String = ""
    var nim : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as pertemuanenam
        v = inflater.inflate(R.layout.frag_mahasiswa,container,false)
        db = thisParent.getDBObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnUbahMhs.setOnClickListener(this)
        v.btnTambahMhs.setOnClickListener(this)
        v.btnCari.setOnClickListener(this)
        v.btnHapusMhs.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.lvMahasiswa.setOnItemClickListener(itemClick)
        return v
    }


    override fun onStart() {
        super.onStart()
        tampilDataMhs("")
        tampilDataProdi()
    }

    fun tampilDataMhs(namaMhs:String) {
        var sql = ""
        if (!namaMhs.trim().equals("")) {
            sql = "select m.nim as _id, m.nama, p.nama_prodi from mhs m, prodi p " +
                    "where m.id_prodi=p.id_prodi and m.nama like '%$namaMhs%'"
        } else {
            sql = "select m.nim as _id, m.nama, p.nama_prodi from mhs m, prodi p " +
                    "where m.id_prodi=p.id_prodi order by m.nama asc"
        }
        val cursor : Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_mahasiswa,cursor,
        arrayOf("_id", "nama", "nama_prodi"), intArrayOf(R.id.txvNimMhs, R.id.txvNamaMhs, R.id.txvProdiMhs), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lvMahasiswa.adapter = lsAdapter

    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val cursor : Cursor = parent.adapter.getItem(position) as Cursor
        nim = cursor.getString(cursor.getColumnIndex("_id"))
        namaMhs = cursor.getString(cursor.getColumnIndex("nama"))
        v.edtNamaMhs.setText(cursor.getString(cursor.getColumnIndex("nama")))
        v.edtNIM.setText(cursor.getString(cursor.getColumnIndex("_id")))
    }

    fun tampilDataProdi(){
        val cursor : Cursor = db.rawQuery("select nama_prodi as _id from prodi order by nama_prodi asc", null)
        spAdapter = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,cursor,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }

    fun tambahMhs(nim:String,namaMhs: String, id_prodi:Int) {
        var sql = "insert into mhs(nim,nama,id_prodi) values(?,?,?)"
        db.execSQL(sql, arrayOf(nim,namaMhs,id_prodi))
        tampilDataMhs("")
    }
    val btnTambahMhsDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi = '$namaProdi'"
        val cursor : Cursor = db.rawQuery(sql, null)
        if (cursor.count > 0){
            cursor.moveToFirst()
            tambahMhs(v.edtNIM.text.toString(),v.edtNamaMhs.text.toString(),
                cursor.getInt(cursor.getColumnIndex("id_prodi")))
            v.edtNIM.setText("")
            v.edtNamaMhs.setText("")
            Toast.makeText(activity, "Tambah Sukses", Toast.LENGTH_SHORT).show()
        }
    }

    val btnUbahDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi = '$namaProdi'"
        val c : Cursor = db.rawQuery(sql, null)
        if (c.count>0){
            c.moveToFirst()
            ubahMhs(v.edtNIM.text.toString(), v.edtNamaMhs.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi")))
            v.edtNIM.setText("")
            v.edtNamaMhs.setText("")
            Toast.makeText(activity, "Ubah Sukses", Toast.LENGTH_SHORT).show()
        }
    }
    fun ubahMhs(nim : String, namaMhs: String, id_prodi : Int) {
        var sql = "update mhs set nama = ?, id_prodi = ? where nim = ?"
        db.execSQL(sql, arrayOf(namaMhs, id_prodi, nim))
        tampilDataMhs("")
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi = '$namaProdi'"
        val c : Cursor = db.rawQuery(sql, null)
        if (c.count>0){
            c.moveToFirst()
            deleteDataMhs(v.edtNIM.text.toString())
            v.edtNIM.setText("")
            v.edtNamaMhs.setText("")
            Toast.makeText(activity, "Hapus Sukses", Toast.LENGTH_SHORT).show()
        }
    }
    fun deleteDataMhs(nim : String) {
        var sql = "delete from mhs where nim = ?"
        db.execSQL(sql, arrayOf(nim))
        tampilDataMhs("")
    }

}
