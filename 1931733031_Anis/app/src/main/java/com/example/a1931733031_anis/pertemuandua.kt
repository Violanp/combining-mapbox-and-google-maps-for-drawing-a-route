package com.example.a1931733031_anis

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_pertemuandua.*

class pertemuandua : AppCompatActivity() {

    val arrayNama = arrayOf("Anis Setiya", "Viola Nataya", "Alissa", "Dayinta", "Laila")
    val arrayHobi = arrayOf("Memancing", "Bercerita", "Menyanyi", "Traveling")

    lateinit var adapterActv : ArrayAdapter<String>
    lateinit var adapterSpin : ArrayAdapter<String>
    lateinit var adapterLv : ArrayAdapter<String>
    lateinit var arrGabungan : ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pertemuandua)

        adapterActv = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayNama)
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayHobi)
        arrGabungan = ArrayList()
        adapterLv = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrGabungan)

        actv.setAdapter(adapterActv)
        sp1.adapter = adapterSpin
        lv1.adapter = adapterLv

        actv.threshold = 2
        actv.setOnItemClickListener { parent, view, position, id ->
            val isi = adapterActv.getItem(position)
            Toast.makeText(this, isi, Toast.LENGTH_SHORT).show()
        }

        sp1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext, "tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Toast.makeText(baseContext, adapterSpin.getItem(position), Toast.LENGTH_SHORT)
                    .show()
            }
        }

        lv1.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(this, adapterLv.getItem(position), Toast.LENGTH_SHORT).show()
        }
        button.setOnClickListener{
            val var1 = actv.text
            val var2 = adapterSpin.getItem(sp1.selectedItemPosition)
            val var3 = "$var1 hobinya $var2"
            arrGabungan.add(var3)
            Toast.makeText(this, var3, Toast.LENGTH_SHORT).show()
            adapterLv.notifyDataSetChanged()
        }
    }
}
