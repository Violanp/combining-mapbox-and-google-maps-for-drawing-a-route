package com.example.a1931733031_anis

import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_pertemuanenam.*
import android.graphics.Color
import androidx.fragment.app.FragmentTransaction

class pertemuanenam : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragProdi: fragmenProdi
    lateinit var fragMahasiswa: fragmentMahasiswa
    lateinit var ft: FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pertemuanenam)

        BottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = fragmenProdi()
        fragMahasiswa = fragmentMahasiswa()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDBObject(): SQLiteDatabase{
        return db
    }


    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.itemProdi -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragmentLayout, fragProdi).commit()
                fragmentLayout.setBackgroundColor(Color.argb(245, 255, 255, 225))
                fragmentLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragmentLayout, fragMahasiswa).commit()
                fragmentLayout.setBackgroundColor(Color.argb(245, 255, 255, 225))
                fragmentLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> fragmentLayout.visibility = View.GONE
        }
        return true
    }
}

