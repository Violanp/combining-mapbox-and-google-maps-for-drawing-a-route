package com.example.a1931733031_anis

import android.content.ClipData
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_pertemuanlima.*

class pertemuanlima : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        var popMenu = PopupMenu(this,v)
        popMenu.menuInflater.inflate(R.menu.menu_popup,popMenu.menu)
        popMenu.setOnMenuItemClickListener{ item ->
            when(item.itemId){
                R.id.itemDownload ->{
                    var webUri = "https://detik.com"
                    var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(webUri))
                    startActivity(intentInternet)
                    Toast.makeText(baseContext, "Masuk Website Detik.com", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.itemUpload ->{
                    Toast.makeText(baseContext, "Upload film", Toast.LENGTH_SHORT).show()
                    true
                }
            }
            false
        }
        popMenu.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pertemuanlima)

        registerForContextMenu(txContextMenu)
        btnPopup.setOnClickListener(this)
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        var mnuInflater = getMenuInflater()
        mnuInflater.inflate(R.menu.menu_context,menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemPaste ->{
                Toast.makeText(this,"Action Paste dipilih", Toast.LENGTH_SHORT).show()
                return true
            }
            R.id.itemDelete ->{
                var snackbar = Snackbar.make(constraintLayout, "Action Delete dipilih", Snackbar.LENGTH_SHORT)
                snackbar.show()
                return true
            }
            R.id.itemCopy ->{
                var alertBuilder = AlertDialog.Builder(this)
                alertBuilder.setTitle("Informasi").setMessage("Action Copy dipilih")
                alertBuilder.setNeutralButton("OK", null)
                alertBuilder.show()
                return true
            }
            else ->{
                Toast.makeText(this, "Tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
                return false
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemPlay ->{
                val intent = Intent (this, anis::class.java)
                startActivity(intent)
                Toast.makeText(this, "Play music", Toast.LENGTH_SHORT).show()
                return true
            }
            R.id.itemPause ->{
                var snackBar = Snackbar.make(constraintLayout, "Pause the music", Snackbar.LENGTH_SHORT)
                snackBar.show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
