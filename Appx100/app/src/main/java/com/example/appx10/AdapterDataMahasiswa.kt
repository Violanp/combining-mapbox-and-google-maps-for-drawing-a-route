package com.example.appx10

import android.graphics.Color
import android.net.Uri
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class AdapterDataMahasiswa (val dataMhs : List<HashMap<String,String>>) : RecyclerView.Adapter<AdapterDataMahasiswa.HolderDataMahasiswa>() {
    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterDataMahasiswa.HolderDataMahasiswa {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_mahasiswa, p0, false)
        return HolderDataMahasiswa(v)
    }

    override fun getItemCount(): Int {
        return dataMhs.size
    }

    override fun onBindViewHolder(p0: AdapterDataMahasiswa.HolderDataMahasiswa, p1: Int) {
        val data = dataMhs.get(p1)
        p0.txNim.setText(data.get("nim"))
        p0.txNama.setText(data.get("nama"))
        p0.txProdi.setText(data.get("nama_prodi"))

        if(p1.rem(2) == 0)p0.cLayout.setBackgroundColor(Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.img)
    }

    class HolderDataMahasiswa (v: View):RecyclerView.ViewHolder(v) {
        val txNim = v.findViewById<TextView>(R.id.txNim)
        val txNama = v.findViewById<TextView>(R.id.txNamaMhs)
        val txProdi = v.findViewById<TextView>(R.id.txNamaProdi)
        val img = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.layoutListMhs)

    }
}