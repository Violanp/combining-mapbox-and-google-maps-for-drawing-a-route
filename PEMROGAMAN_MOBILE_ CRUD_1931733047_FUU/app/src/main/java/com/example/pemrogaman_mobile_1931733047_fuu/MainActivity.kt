package com.example.pemrogaman_mobile_1931733047_fuu

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragProdi:fragmentProdi
    lateinit var fragMahasiswa:fragmentMahasiswa
    lateinit var ft: FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bnv1.setOnNavigationItemSelectedListener(this)
        fragProdi = fragmentProdi()
        fragMahasiswa = fragmentMahasiswa()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject() : SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemProdi -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragmentLayout, fragProdi).commit()
                fragmentLayout.setBackgroundColor(Color.argb(245,255,255,225))
                fragmentLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragmentLayout, fragMahasiswa).commit()
                fragmentLayout.setBackgroundColor(Color.argb(245,225,255,255))
                fragmentLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> fragmentLayout.visibility = View.GONE
        }
        return true
    }
}
