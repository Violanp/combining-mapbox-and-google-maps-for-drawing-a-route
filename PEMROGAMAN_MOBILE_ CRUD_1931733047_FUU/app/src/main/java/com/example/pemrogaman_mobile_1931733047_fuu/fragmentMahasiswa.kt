package com.example.pemrogaman_mobile_1931733047_fuu

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_mahasiswa.*
import kotlinx.android.synthetic.main.frag_mahasiswa.view.*
import kotlinx.android.synthetic.main.frag_mahasiswa.view.spinner
import kotlinx.android.synthetic.main.item_data_mhs.*

class fragmentMahasiswa:Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener{

    lateinit var  thisParent: MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var nim : String = ""
    var namaMhs : String = ""
    var namaProdi : String = ""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_mahasiswa, container, false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnTambahMhs.setOnClickListener(this)
        v.btnUbahMhs.setOnClickListener(this)
        v.btnHapusMhs.setOnClickListener(this)
        v.btnCari.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.lsMhs.setOnItemClickListener(itemClick)

        return v
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahMhs -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukan sudah benar?")
                    .setPositiveButton("Ya",btnTambahMhs)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnUbahMhs -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukan sudah benar?")
                    .setPositiveButton("Ya",btnUbahMhs)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnHapusMhs -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Yakin akan menghapus data ini?")
                    .setPositiveButton("Ya",btnHapusMhs)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnCari -> {
                showDataMhs(edNamaMhs.text.toString())
            }
        }
    }

    override fun onStart() {
        super.onStart()
        showDataProdi()
        showDataMhs("")
    }

    fun showDataMhs(namaMhs: String){
        var sql=""
        if(!namaMhs.trim().equals("")){
            sql = "select m.nim as _id, m.nama, p.nama_prodi from mhs m, prodi p " +
                    "where m.id_prodi=p.id_prodi and m.nama like '%$namaMhs%'"
        } else{
            sql = "select m.nim as _id, m.nama, p.nama_prodi from mhs m, prodi p " +
                    "where m.id_prodi=p.id_prodi order by m.nama asc"
        }

        val c : Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_mhs, c,
            arrayOf("_id","nama","nama_prodi"), intArrayOf(R.id.txNimMhs, R.id.txNamaMhs, R.id.txNamaPS),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsMhs.adapter = lsAdapter
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        nim = c.getString(c.getColumnIndex("_id"))
        namaMhs = c.getString(c.getColumnIndex("nama"))
        v.edNimMhs.setText(c.getString(c.getColumnIndex("_id")))
        v.edNamaMhs.setText(c.getString(c.getColumnIndex("nama")))
    }

    fun showDataProdi(){
        val c : Cursor = db.rawQuery("select nama_prodi as _id from prodi order by nama_prodi asc",null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }

    fun TambahDataMhs(nim: String, namaMhs: String, id_prodi: Int){
        var sql = "insert into mhs(nim, nama, id_prodi) values(?,?,?)"
        db.execSQL(sql, arrayOf(nim,namaMhs,id_prodi))
        showDataMhs("")
    }

    val btnTambahMhs = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi='$namaProdi'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
            TambahDataMhs(v.edNimMhs.text.toString(), v.edNamaMhs.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi")))
            v.edNimMhs.setText("")
            v.edNamaMhs.setText("")
            Toast.makeText(thisParent,"Berhasil menambah data", Toast.LENGTH_SHORT).show()
        }
    }

    fun UbahDataMhs(nim: String, namaMhs: String, id_prodi: Int){
        var sql = "update mhs set nama=?, id_prodi=? where nim=?"
        db.execSQL(sql, arrayOf(namaMhs,id_prodi, nim))
        showDataMhs("")
    }

    val btnUbahMhs = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi='$namaProdi'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
            UbahDataMhs(v.edNimMhs.text.toString(), v.edNamaMhs.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi")))
            v.edNimMhs.setText("")
            v.edNamaMhs.setText("")
            Toast.makeText(thisParent,"Berhasil mengubah data",Toast.LENGTH_SHORT).show()
        }
    }

    fun HapusDataMhs(nim: String){
        db.delete("mhs", "nim = $nim", null)
        showDataMhs("")
    }

    val btnHapusMhs = DialogInterface.OnClickListener { dialog, which ->
        HapusDataMhs(nim)
        v.edNimMhs.setText("")
        v.edNamaMhs.setText("")
        Toast.makeText(thisParent,"Berhasil menghapus data",Toast.LENGTH_SHORT).show()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaProdi = c.getString(c.getColumnIndex("_id"))
    }
}