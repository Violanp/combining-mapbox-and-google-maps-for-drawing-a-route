package com.example.pemrogaman_mobile_1931733047_fuu

import android.R.drawable.ic_dialog_info
import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.ContentObservable
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_mahasiswa.view.*
import kotlinx.android.synthetic.main.frag_prodi.view.*


class fragmentProdi : Fragment(), View.OnClickListener {
    lateinit var thisParent:MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter: ListAdapter
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    var idProdi : String = ""
    val namaProdi: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDbObject()

        v = inflater.inflate(R.layout.frag_prodi, container, false)
        v.btnUbah.setOnClickListener(this)
        v.btnTambah.setOnClickListener(this)
        v.btnHapus.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsProdi.setOnItemClickListener(itemClick)

        return v
    }

    fun  showDataProdi(){
        val cursor : Cursor = db.query("prodi", arrayOf("nama_prodi", "id_prodi as _id"),
            null, null, null, null, "nama_prodi asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_prodi,cursor,
            arrayOf("_id", "nama_prodi"), intArrayOf(R.id.txtIdProdi, R.id.txtNamaProdi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsProdi.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataProdi()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambah ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukan sudah benar?")
                    .setIcon(ic_dialog_info)
                    .setPositiveButton("Ya", btnTambahDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnUbah ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukan sudah benar?")
                    .setIcon(ic_dialog_info)
                    .setPositiveButton("Ya", btnUbahDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnHapus ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(ic_dialog_info)
                    .setPositiveButton("Ya", btnHapusDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()

            }
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        idProdi = c.getString(c.getColumnIndex("_id"))
        v.editProdi.setText(c.getString(c.getColumnIndex("nama_prodi")))
    }

    fun TambahProdi(namaProdi : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_prodi", namaProdi)
        db.insert("prodi", null, cv)
        showDataProdi()
    }

    fun UbahProdi(namaProdi: String, idProdi: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_prodi", namaProdi)
        db.update("prodi",cv,"id_prodi = $idProdi", null)
        showDataProdi()
    }

    fun HapusProdi(idProdi: String){
        db.delete("prodi", "id_prodi = $idProdi", null)
        showDataProdi()
    }

    val btnTambahDialog = DialogInterface.OnClickListener { dialog, which ->
        TambahProdi(v.editProdi.text.toString())
        Toast.makeText(thisParent,"Berhasil menambah data",Toast.LENGTH_SHORT).show()
        v.editProdi.setText("")
    }

    val btnUbahDialog = DialogInterface.OnClickListener { dialog, which ->
        UbahProdi(v.editProdi.text.toString(),idProdi)
        v.editProdi.setText("")
        Toast.makeText(thisParent,"Berhasil mengubah data",Toast.LENGTH_SHORT).show()
    }

    val btnHapusDialog = DialogInterface.OnClickListener { dialog, which ->
        HapusProdi(idProdi)
        v.editProdi.setText("")
        Toast.makeText(thisParent,"Berhasil menghapus data",Toast.LENGTH_SHORT).show()
    }
}