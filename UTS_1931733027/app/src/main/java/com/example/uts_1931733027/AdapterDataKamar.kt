package com.example.uts_1931733047

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.uts_1931733027.R

class AdapterDataKamar (val dataKamar : List<HashMap<String,String>>) : RecyclerView.Adapter<AdapterDataKamar.HolderDataKamar>() {
    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterDataKamar.HolderDataKamar {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_kamar, p0, false)
        return HolderDataKamar(v)
    }

    override fun getItemCount(): Int {
        return dataKamar.size
    }

    override fun onBindViewHolder(p0: AdapterDataKamar.HolderDataKamar, p1: Int) {
        val data = dataKamar.get(p1)
        p0.txIdKamar.setText(data.get("id_kamar"))
        p0.txNamaKamar.setText(data.get("nama_kamar"))

        if(p1.rem(2) == 0)p0.cLayout.setBackgroundColor(Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

    }

    class HolderDataKamar (v: View):RecyclerView.ViewHolder(v) {
        val txIdKamar = v.findViewById<TextView>(R.id.txIdKamar)
        val txNamaKamar = v.findViewById<TextView>(R.id.txNamaKamar)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.layoutItemKamar)

    }
}