package com.example.uts_1931733047

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.uts_1931733027.R
import com.squareup.picasso.Picasso

class AdapterDataPasien (val dataPasien : List<HashMap<String,String>>) : RecyclerView.Adapter<AdapterDataPasien.HolderDataPasien>() {
    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterDataPasien.HolderDataPasien {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_pasien, p0, false)
        return HolderDataPasien(v)
    }

    override fun getItemCount(): Int {
        return dataPasien.size
    }

    override fun onBindViewHolder(p0: AdapterDataPasien.HolderDataPasien, p1: Int) {
        val data = dataPasien.get(p1)
        p0.txNamaPasien.setText(data.get("nama"))
        p0.txAlamat.setText(data.get("alamat"))
        p0.txJK.setText(data.get("jenis_kelamin"))
        p0.txKeluhan.setText(data.get("keluhan"))
        p0.txNoKmr.setText(data.get("nama_kamar"))
        p0.txTanggal.setText(data.get("created_at"))

        if(p1.rem(2) == 0)p0.cLayout.setBackgroundColor(Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.img)
    }

    class HolderDataPasien (v: View):RecyclerView.ViewHolder(v) {
        val txNamaPasien = v.findViewById<TextView>(R.id.txNama)
        val txAlamat = v.findViewById<TextView>(R.id.txAlamat)
        val txJK = v.findViewById<TextView>(R.id.txJK)
        val txKeluhan = v.findViewById<TextView>(R.id.txKeluhan)
        val txNoKmr = v.findViewById<TextView>(R.id.txNoKmr)
        val txTanggal = v.findViewById<TextView>(R.id.txTanggal)
        val img = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.layoutItemPasien)

    }
}