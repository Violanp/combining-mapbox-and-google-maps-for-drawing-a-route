package com.example.uts_1931733027

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context):SQLiteOpenHelper(context,DB_Name, null,DB_Ver){
    override fun onCreate(db: SQLiteDatabase?) {
        val tPasien = "create table pasien(id_pasien integer primary key autoincrement, nama text not null, alamat text not null, jenis_kelamin text not null, keluhan text not null, id_kamar integer not null, created_at text not null)"
        val tKmr = "create table kamar(id_kamar integer primary key autoincrement, nama_kamar text not null)"
        val insPasien= "insert into pasien(nama,alamat,jenis_kelamin,keluhan,id_kamar,created_at) values" +
                "('Viola Nataya','Kediri','P','Batuk Pilek','1','10-02-2020 13:00')," +
                "('Anis Setya','Tarokan','P','Pusing Mual','2','12-02-2020 18:00')," +
                "('Dayinta','Balowerti','P','Batuk','3','13-02-2020 15:00')," +
                "('Fuad Noor','Tarokan','L','Pusing','4','15-02-2020 7:00')," +
                "('Dika Andika','Ngasem','L','Mual','5','17-02-2020 8:00')," +
                "('Irvan Dwi','Prambon','L','Batuk Pilek Demam','6','18-02-2020 16:00')," +
                "('Heriana Adawiyah','Pare','P','Mual Pilek','7','19-02-2020 17:00')," +
                "('Dody Ilham','Wates','L','Pilek','8','20-02-2020 19:00')," +
                "('Alissa Qutnanda','Paron','P','Pusing Mual','9','21-02-2020 19:00')," +
                "('Risma Ayu','Blitar','P','Batuk Pilek Mual','10','22-02-2020 20:00')"
        val insKmr= "insert into kamar(nama_kamar) values('Mawar'),('Melati'),('Bogenviel'),('Tulip'),('Kenanga'),('Matahri'),('Flamboyan'),('Terate'),('Sakura'),('Anggrek')"
        db?.execSQL(tPasien)
        db?.execSQL(tKmr)
        db?.execSQL(insPasien)
        db?.execSQL(insKmr)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    companion object {
        val DB_Name = "rumah_sakit"
        val DB_Ver = 1
    }
}