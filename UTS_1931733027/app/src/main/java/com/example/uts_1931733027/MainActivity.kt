package com.example.uts_1931733027

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.example.uts_1931733027.QRCodeActivity
import com.example.uts_1931733027.ShareLocActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener{

    lateinit var adapterActv : ArrayAdapter<String>
    val arrayNamaP = arrayOf("Viola Nataya","Anis Setiya","Dayinta","Fuad Noor","Dika Andika","Irvan Dwi","Heriana Adawiyah","Dody Ilham","Alissa Qutnanda","Risma Ayu")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnCari.setOnClickListener(this)
        btnOption.setOnClickListener(this)
        btnKelola.setOnClickListener(this)
        btngps.setOnClickListener(this)
        btnscan.setOnClickListener(this)
        btnvideo.setOnClickListener(this)
        adapterActv = ArrayAdapter(this, android.R.layout.simple_list_item_1,arrayNamaP)

        edCariP.setAdapter(adapterActv)
        edCariP.threshold = 2
        edCariP.setOnItemClickListener { parent, view, position, id ->
            val isi = adapterActv.getItem(position)
            Toast.makeText(this,isi, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClick(v: View?) {
        var popMenu = PopupMenu(this,v)
        popMenu.menuInflater.inflate(R.menu.menu_option,popMenu.menu)
        popMenu.setOnMenuItemClickListener{ item ->
            when(item.itemId){
                R.id.itemLink ->{
                    var webUri = "https://kemkes.go.id"
                    var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(webUri))
                    startActivity(intentInternet)
                    Toast.makeText(this,"Membuka Link Kemenkes", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.itemGallery ->{
                    var intentGal = Intent()
                    intentGal.setType("image/*") //filter application than can handle image
                    intentGal.setAction(Intent.ACTION_GET_CONTENT)
                    startActivity(Intent.createChooser(intentGal, "Pilih Gambar ... "))
                    Toast.makeText(this,"Membuka Gallery", Toast.LENGTH_SHORT).show()
                    true
                }
            }
            false
        }

        when(v?.id){
            R.id.btnCari -> {
                var intent = Intent(this, PasienActivity::class.java)
                intent.putExtra("cariP",edCariP.getText().toString())
                startActivity(intent)
            }
            R.id.btnKelola -> {
                var intent = Intent(this, PasienActivity::class.java)
                startActivity(intent)
            }
            R.id.btnOption -> {
                popMenu.show()
            }
        R.id.btnscan -> {
            var i = Intent(this, QRCodeActivity::class.java)
            startActivity(i)
        }
        R.id.btnvideo -> {
            var i = Intent(this, VideoActivity::class.java)
            startActivity(i)
        }
        R.id.btngps -> {
            var i = Intent(this, ShareLocActivity::class.java)
            startActivity(i)
        }
        }
    }
}
