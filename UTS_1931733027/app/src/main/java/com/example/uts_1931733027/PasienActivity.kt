package com.example.uts_1931733027

import android.app.*
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.uts_1931733047.AdapterDataPasien
import com.example.uts_1931733047.MediaHelper
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_pasien.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class PasienActivity : AppCompatActivity(), View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener  {

    var tahun = 0
    var bulan = 0
    var hari = 0
    var jam = 0
    var menit = 0

    lateinit var dialog : AlertDialog.Builder
    lateinit var mediaHelper : MediaHelper
    lateinit var pasienAdapter : AdapterDataPasien
    lateinit var kamarAdapter : ArrayAdapter<String>
    var daftarPasien = mutableListOf<HashMap<String, String>>()
    var daftarKamar = mutableListOf<String>()
    val url = "http://192.168.1.102/rumah_sakit/show_data.php"
    val url2 = "http://192.168.1.102/rumah_sakit/get_nama_kamar.php"
    val url3 = "http://192.168.1.102/rumah_sakit/query_upd_del_ins.php"
    var imStr = ""
    var pilihKamar = ""
    var namaFile = ""
    var fileUri = Uri.parse("")

    lateinit var fragKamar : fragmentKamar
    lateinit var fragAbout : fragmentAbout
    lateinit var ft: FragmentTransaction

    var jk1 : String = ""
    var keluhan1 : String = ""
    var keluhan2 : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pasien)
        val cal : Calendar = Calendar.getInstance()
        bulan = cal.get(Calendar.MONTH)
        hari = cal.get(Calendar.DAY_OF_MONTH)
        tahun = cal.get(Calendar.YEAR)
        jam = cal.get(Calendar.HOUR)
        menit = cal.get(Calendar.MINUTE)

        txDateTime.text = "$hari-${bulan+1}-$tahun $jam:$menit"

        Dpd1.setOnClickListener(this)
        Tpd1.setOnClickListener(this)

        fragKamar = fragmentKamar()
        fragAbout = fragmentAbout()
        bnv1.setOnNavigationItemSelectedListener(this)
        edNama.setText(getIntent().getStringExtra("cariP"));
        dialog = AlertDialog.Builder(this)
        btnTambah.setOnClickListener(this)
        btnUbah.setOnClickListener(this)
        btnHapus.setOnClickListener(this)
        btnCari.setOnClickListener(this)

        rg1.setOnCheckedChangeListener{ group, checkedId ->
            when(checkedId){
                R.id.rb1 -> jk1 = "L"
                R.id.rb2 -> jk1 = "P"

            }
        }

        pasienAdapter = AdapterDataPasien(daftarPasien)
        rvPasien.layoutManager = LinearLayoutManager(this)
        rvPasien.adapter = pasienAdapter
        mediaHelper = MediaHelper(this)

        kamarAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarKamar)
        spinner.adapter = kamarAdapter
        spinner.onItemSelectedListener = itemSelected
        imgP.setOnClickListener(this)
        rvPasien.addOnItemTouchListener(itemTouch)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception){
            e.printStackTrace()
        }
    }

    var timeChangeDialog = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
        txDateTime.text = "$hari-$bulan-$tahun $hourOfDay:$minute"
        jam = hourOfDay
        menit = minute
        Toast.makeText(this,"Jam Berhasil Diinputkan", Toast.LENGTH_SHORT).show()
    }

    var dateChangeDialog = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
        txDateTime.text = "$dayOfMonth-${month+1}-$year $jam:$menit"
        tahun = year
        bulan = month
        hari = dayOfMonth
        Toast.makeText(this,"Tanggal Berhasil Diinputkan", Toast.LENGTH_SHORT).show()
    }

    override fun onCreateDialog(id: Int): Dialog {
        when(id){
            10 -> return TimePickerDialog(this, timeChangeDialog,jam,menit, true)
            20 -> return DatePickerDialog(this, dateChangeDialog,tahun,bulan,hari)
        }
        return super.onCreateDialog(id)
    }

    override fun onClick(v: View?) {
        var popMenu1 = PopupMenu(this,v)
        popMenu1.menuInflater.inflate(R.menu.menu_pasien,popMenu1.menu)
        popMenu1.setOnMenuItemClickListener{ item ->
            when(item.itemId){
                R.id.itemAmbilFoto ->{
                    RequestPermission()
                }
                R.id.itemPilihGallery ->{
                    val intent = Intent()
                    intent.setType("image/*")
                    intent.setAction(Intent.ACTION_GET_CONTENT)
                    startActivityForResult(intent, mediaHelper.getRcGallery())
                }
            }
            false
        }
        when(v?.id){
            R.id.Dpd1 -> {
                showDialog(20)
                Toast.makeText(this,"Input Tanggal", Toast.LENGTH_SHORT).show()
            }
            R.id.Tpd1 -> {
                showDialog(10)
                Toast.makeText(this,"Input Jam", Toast.LENGTH_SHORT).show()
            }
            R.id.btnTambah -> {
                if(cb1.isChecked) keluhan1 = "Panas"
                if(cb2.isChecked) keluhan2 = "Batuk"
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUbah -> {
                if(cb1.isChecked){
                    keluhan1 = "Panas"
                }else{
                    keluhan1 = ""
                }
                if(cb2.isChecked){
                    keluhan2 = "Batuk"
                }else{
                    keluhan2 = ""
                }
                queryInsertUpdateDelete("update")
                showDataPasien("")
            }
            R.id.btnHapus -> {
                queryInsertUpdateDelete("delete")
                showDataPasien("")
            }
            R.id.btnCari -> {
                showDataPasien(edNama.text.toString())
            }
            R.id.imgP->{
                popMenu1.show()
            }
        }
   }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemKmr -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragmentLayout, fragKamar).commit()
                fragmentLayout.visibility = View.VISIBLE
                Dpd1.visibility = View.GONE
                Tpd1.visibility = View.GONE
                btnHapus.visibility = View.GONE
                btnTambah.visibility = View.GONE
                btnUbah.visibility = View.GONE
            }
            R.id.itemPasien -> {
                fragmentLayout.visibility = View.GONE
                Dpd1.visibility = View.VISIBLE
                Tpd1.visibility = View.VISIBLE
                btnHapus.visibility = View.VISIBLE
                btnTambah.visibility = View.VISIBLE
                btnUbah.visibility = View.VISIBLE
            }
            R.id.itemAbout -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragmentLayout, fragAbout).commit()
                fragmentLayout.visibility = View.VISIBLE
                Dpd1.visibility = View.GONE
                Tpd1.visibility = View.GONE
                btnHapus.visibility = View.GONE
                btnTambah.visibility = View.GONE
                btnUbah.visibility = View.GONE
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        showDataPasien("")
        getNamaKamar()
    }

    fun showDataPasien(namaPasien : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarPasien.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var pasien = HashMap<String,String>()
                    pasien.put("nama", jsonObject.getString("nama"))
                    pasien.put("alamat", jsonObject.getString("alamat"))
                    pasien.put("jenis_kelamin", jsonObject.getString("jenis_kelamin"))
                    pasien.put("keluhan", jsonObject.getString("keluhan"))
                    pasien.put("nama_kamar", jsonObject.getString("nama_kamar"))
                    pasien.put("created_at", jsonObject.getString("created_at"))
                    pasien.put("url", jsonObject.getString("url"))
                    daftarPasien.add(pasien)
                }
                pasienAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "terjadi kesalahan koneksi", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama",namaPasien)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner.setSelection(0)
            pilihKamar = daftarKamar.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihKamar = daftarKamar.get(position)
        }

    }

    val itemTouch= object : RecyclerView.OnItemTouchListener{
        override fun onTouchEvent(p0: RecyclerView, p1: MotionEvent) {}

        override fun onInterceptTouchEvent(p0: RecyclerView, p1: MotionEvent): Boolean {
            val view = p0.findChildViewUnder(p1.x,p1.y)
            val tag = p0.getChildAdapterPosition(view!!)
            val pos = daftarKamar.indexOf(daftarPasien.get(tag).get("nama_kamar"))

            spinner.setSelection(pos)
            edNama.setText(daftarPasien.get(tag).get("nama").toString())
            edAlamat.setText(daftarPasien.get(tag).get("alamat").toString())
            Picasso.get().load(daftarPasien.get(tag).get("url")).into(imgP)

            return false
        }

        override fun onRequestDisallowInterceptTouchEvent(p0: Boolean) {}

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()) {
                imStr = data!!.data?.let { mediaHelper.getBitmapToString(it, imgP) }.toString()
            }else if(requestCode == mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToString2(imgP,fileUri)
                namaFile = mediaHelper.getMyFileName()
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    showDataPasien("")
                    Toast.makeText(this,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Operasi Gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Koneksi Gagal",Toast.LENGTH_LONG).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val namaFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
                when (mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama",edNama.text.toString())
                        hm.put("alamat",edAlamat.text.toString())
                        hm.put("jenis_kelamin",jk1)
                        hm.put("keluhan",keluhan1+" "+keluhan2)
                        hm.put("nama_kamar",pilihKamar)
                        hm.put("created_at",txDateTime.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namaFile)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nama",edNama.text.toString())
                        hm.put("alamat",edAlamat.text.toString())
                        hm.put("jenis_kelamin",jk1)
                        hm.put("keluhan",keluhan1+" "+keluhan2)
                        hm.put("nama_kamar",pilihKamar)
                        hm.put("created_at",txDateTime.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namaFile)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("alamat",edAlamat.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaKamar(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarKamar.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKamar.add(jsonObject.getString("nama_kamar"))
                }
                kamarAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun RequestPermission() = runWithPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
        android.Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }

}
