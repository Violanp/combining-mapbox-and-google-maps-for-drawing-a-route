package com.example.uts_1931733027

import android.content.pm.ActivityInfo
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import android.widget.MediaController
import com.example.uts_1931733027.R
import kotlinx.android.synthetic.main.activity_video.*

class VideoActivity : AppCompatActivity() {

    val daftarVideo = intArrayOf(R.raw.`video1`)
    var posVidSkrg = 0

    lateinit var mediaController: MediaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        mediaController = MediaController(this)
        mediaController.setPrevNextListeners(nextVid,prevVid)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        videoSet(posVidSkrg)
    }

    var nextVid = View.OnClickListener { v: View ->
        if(posVidSkrg<(daftarVideo.size-1)) posVidSkrg++
        else posVidSkrg = 0
        videoSet(posVidSkrg)
    }

    var prevVid = View.OnClickListener { v: View ->
        if(posVidSkrg>0) posVidSkrg--
        else posVidSkrg = daftarVideo.size-1
        videoSet(posVidSkrg)
    }

    fun videoSet(pos:Int){
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarVideo[pos]))
    }
}
