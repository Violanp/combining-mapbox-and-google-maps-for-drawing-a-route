package com.example.uts_1931733027

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class fragmentAbout : Fragment() {
    lateinit var thisParent : PasienActivity
    lateinit var v : View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as PasienActivity

        v = inflater.inflate(R.layout.frag_about, container, false)
        return v
    }
}