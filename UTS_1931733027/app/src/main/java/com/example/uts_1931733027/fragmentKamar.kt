package com.example.uts_1931733027

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.uts_1931733047.AdapterDataKamar
import kotlinx.android.synthetic.main.activity_pasien.view.*
import kotlinx.android.synthetic.main.frag_kamar.*
import kotlinx.android.synthetic.main.frag_kamar.view.*
import kotlinx.android.synthetic.main.frag_kamar.view.btnHapus
import kotlinx.android.synthetic.main.frag_kamar.view.btnTambah
import kotlinx.android.synthetic.main.frag_kamar.view.btnUbah
import kotlinx.android.synthetic.main.frag_kamar.view.edNamaKmr
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap

class fragmentKamar : Fragment(), View.OnClickListener {
    lateinit var thisParent : PasienActivity
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    lateinit var kamarAdapter : AdapterDataKamar
    var daftarKamar = mutableListOf<HashMap<String, String>>()
    val url = "http://192.168.1.102/rumah_sakit/show_data_kamar.php"
    val url2 = "http://192.168.1.102/rumah_sakit/query_upd_del_ins_kamar.php"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as PasienActivity

        v = inflater.inflate(R.layout.frag_kamar, container, false)
        v.btnUbah.setOnClickListener(this)
        v.btnTambah.setOnClickListener(this)
        v.btnHapus.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)

        kamarAdapter = AdapterDataKamar(daftarKamar)
        v.rvKamar.layoutManager = LinearLayoutManager(thisParent)
        v.rvKamar.adapter = kamarAdapter

        v.rvKamar.addOnItemTouchListener(itemTouch)

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataKamar()
    }

    fun showDataKamar(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarKamar.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var kmr = HashMap<String,String>()
                    kmr.put("id_kamar", jsonObject.getString("id_kamar"))
                    kmr.put("nama_kamar", jsonObject.getString("nama_kamar"))
                    daftarKamar.add(kmr)
                }
                kamarAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent, "terjadi kesalahan koneksi", Toast.LENGTH_SHORT).show()
            }
        )
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    val itemTouch= object : RecyclerView.OnItemTouchListener{
        override fun onTouchEvent(p0: RecyclerView, p1: MotionEvent) {}

        override fun onInterceptTouchEvent(p0: RecyclerView, p1: MotionEvent): Boolean {
            val view = p0.findChildViewUnder(p1.x,p1.y)
            val tag = p0.getChildAdapterPosition(view!!)

            edNamaKmr.setText(daftarKamar.get(tag).get("nama_kamar").toString())
            textView7.setText(daftarKamar.get(tag).get("id_kamar").toString())

            return false
        }

        override fun onRequestDisallowInterceptTouchEvent(p0: Boolean) {}

    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    showDataKamar()
                    Toast.makeText(thisParent,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(thisParent,"Operasi Gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Koneksi Gagal",Toast.LENGTH_LONG).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when (mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_kamar",edNamaKmr.text.toString())
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("id_kamar",textView7.text.toString())
                        hm.put("nama_kamar",edNamaKmr.text.toString())
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_kamar",textView7.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambah ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUbah ->{
                queryInsertUpdateDelete("update")
                showDataKamar()
            }
            R.id.btnHapus ->{
                queryInsertUpdateDelete("delete")
                showDataKamar()
            }
        }
    }
}