package com.example.uts_1931733047

import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.uts_1931733042.R
import com.squareup.picasso.Picasso

class AdapterDataKmr (val dataKmr : List<HashMap<String,String>>) : RecyclerView.Adapter<AdapterDataKmr.HolderDataKmr>() {
    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterDataKmr.HolderDataKmr {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_kamera, p0, false)
        return HolderDataKmr(v)
    }

    override fun getItemCount(): Int {
        return dataKmr.size
    }

    override fun onBindViewHolder(p0: AdapterDataKmr.HolderDataKmr, p1: Int) {
        val data = dataKmr.get(p1)
        p0.txNama.setText(data.get("nama_kmr"))
        p0.txType.setText(data.get("type_kmr"))
        p0.txWarna.setText(data.get("warna_kmr"))
        p0.txFitur.setText(data.get("fitur_kmr"))
        p0.txMerk.setText(data.get("merk_kmr"))
        p0.txTanggal.setText(data.get("created_at"))

        if(p1.rem(2) == 0)p0.cLayout.setBackgroundColor(Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.img)
    }

    class HolderDataKmr (v: View):RecyclerView.ViewHolder(v) {
        val txNama = v.findViewById<TextView>(R.id.txNamaKmr)
        val txType = v.findViewById<TextView>(R.id.txTypeKmr)
        val txWarna = v.findViewById<TextView>(R.id.txWarna)
        val txFitur = v.findViewById<TextView>(R.id.txFitur)
        val txMerk = v.findViewById<TextView>(R.id.txMerkKmr)
        val txTanggal = v.findViewById<TextView>(R.id.txTanggal)
        val img = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.layoutItemKmr)

    }
}