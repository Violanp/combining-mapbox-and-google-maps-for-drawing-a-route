package com.example.uts_1931733047

import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.uts_1931733042.R
import com.squareup.picasso.Picasso

class AdapterDataMerk (val dataMerk : List<HashMap<String,String>>) : RecyclerView.Adapter<AdapterDataMerk.HolderDataMerk>() {
    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterDataMerk.HolderDataMerk {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_merk, p0, false)
        return HolderDataMerk(v)
    }

    override fun getItemCount(): Int {
        return dataMerk.size
    }

    override fun onBindViewHolder(p0: AdapterDataMerk.HolderDataMerk, p1: Int) {
        val data = dataMerk.get(p1)
        p0.txIdMerk.setText(data.get("id_merk"))
        p0.txNamaMerk.setText(data.get("nama_merk"))

        if(p1.rem(2) == 0)p0.cLayout.setBackgroundColor(Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

    }

    class HolderDataMerk (v: View):RecyclerView.ViewHolder(v) {
        val txIdMerk = v.findViewById<TextView>(R.id.txIdMerk)
        val txNamaMerk = v.findViewById<TextView>(R.id.txNamaMerk)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.layoutItemMerk)

    }
}