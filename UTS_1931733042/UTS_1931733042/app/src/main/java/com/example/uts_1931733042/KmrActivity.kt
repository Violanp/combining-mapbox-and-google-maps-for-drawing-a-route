package com.example.uts_1931733042

import android.app.*
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.uts_1931733047.AdapterDataKmr
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_kamera.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class KmrActivity : AppCompatActivity(), View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener{

    var tahun = 0
    var bulan = 0
    var hari = 0
    var jam = 0
    var menit = 0

    lateinit var mediaHelper : MediaHelper
    lateinit var kmrAdapter : AdapterDataKmr
    lateinit var merkAdapter : ArrayAdapter<String>
    var daftarKmr = mutableListOf<HashMap<String, String>>()
    var daftarMerk = mutableListOf<String>()
    val url = "http://192.168.43.246/irvan_kamera/show_data.php"
    val url2 = "http://192.168.43.246/irvan_kamera/get_nama_merk.php"
    val url3 = "http://192.168.43.246/irvan_kamera/query_upd_del_ins.php"
    var imStr = ""
    var pilihMerk = ""
    var namaFile = ""
    var fileUri = Uri.parse("")

    lateinit var dialog : AlertDialog.Builder
    lateinit var fragMerk : fragmentMerk
    lateinit var fragInfo : fragmentInfo
    lateinit var ft: FragmentTransaction

    var warnaKmr1 : String = ""
    var fiturKmr1 : String = ""
    var fiturKmr2 : String = ""
    var fiturKmr3 : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kamera)
        val cal : Calendar = Calendar.getInstance()
        bulan = cal.get(Calendar.MONTH)
        hari = cal.get(Calendar.DAY_OF_MONTH)
        tahun = cal.get(Calendar.YEAR)
        jam = cal.get(Calendar.HOUR)
        menit = cal.get(Calendar.MINUTE)

        txDateTime.text = "$hari-${bulan+1}-$tahun $jam:$menit"

        Dpd1.setOnClickListener(this)
        Tpd1.setOnClickListener(this)

        fragMerk = fragmentMerk()
        fragInfo = fragmentInfo()
        bnv1.setOnNavigationItemSelectedListener(this)
        edNamakmr.setText(getIntent().getStringExtra("cariKmr"));
        dialog = AlertDialog.Builder(this)
        btnTambah.setOnClickListener(this)
        btnUbah.setOnClickListener(this)
        btnHapus.setOnClickListener(this)
        btnCari.setOnClickListener(this)

        rg1.setOnCheckedChangeListener{ group, checkedId ->
            when(checkedId){
                R.id.rb1 -> warnaKmr1 = "Hitam"
                R.id.rb2 -> warnaKmr1 = "Putih"
                R.id.rb3 -> warnaKmr1 = "Merah"
            }
        }

        kmrAdapter = AdapterDataKmr(daftarKmr)
        rvKmr.layoutManager = LinearLayoutManager(this)
        rvKmr.adapter = kmrAdapter
        mediaHelper = MediaHelper(this)

        merkAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarMerk)
        spinner.adapter = merkAdapter
        spinner.onItemSelectedListener = itemSelected
        imgKmr.setOnClickListener(this)
        rvKmr.addOnItemTouchListener(itemTouch)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception){
            e.printStackTrace()
        }
    }

    var timeChangeDialog = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
        txDateTime.text = "$hari-$bulan-$tahun $hourOfDay:$minute"
        jam = hourOfDay
        menit = minute
        Toast.makeText(this,"Berhasil input tanggal", Toast.LENGTH_SHORT).show()
    }

    var dateChangeDialog = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
        txDateTime.text = "$dayOfMonth-${month+1}-$year $jam:$menit"
        tahun = year
        bulan = month
        hari = dayOfMonth
        Toast.makeText(this,"Berhasil input jam", Toast.LENGTH_SHORT).show()
    }

    override fun onCreateDialog(id: Int): Dialog {
        when(id){
            10 -> return TimePickerDialog(this, timeChangeDialog,jam,menit, true)
            20 -> return DatePickerDialog(this, dateChangeDialog,tahun,bulan,hari)
        }
        return super.onCreateDialog(id)
    }

    override fun onClick(v: View?) {
        var popMenu1 = PopupMenu(this,v)
        popMenu1.menuInflater.inflate(R.menu.menu_camera,popMenu1.menu)
        popMenu1.setOnMenuItemClickListener{ item ->
            when(item.itemId){
                R.id.itemAmbilFoto ->{
                    RequestPermission()
                }
                R.id.itemPilihGallery ->{
                    val intent = Intent()
                    intent.setType("image/*")
                    intent.setAction(Intent.ACTION_GET_CONTENT)
                    startActivityForResult(intent, mediaHelper.getRcGallery())
                }
            }
            false
        }

        when(v?.id){
            R.id.Dpd1 -> {
                showDialog(20)
                Toast.makeText(this,"Input Tanggal", Toast.LENGTH_SHORT).show()
            }
            R.id.Tpd1 -> {
                showDialog(10)
                Toast.makeText(this,"Input Jam", Toast.LENGTH_SHORT).show()
            }
            R.id.btnTambah -> {
                if(cb1.isChecked) fiturKmr1 = "WIFI"
                if(cb2.isChecked) fiturKmr2 = "Tahan goyang"
                if(cb3.isChecked) fiturKmr3 = "Sensor"
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUbah -> {
                if(cb1.isChecked){
                    fiturKmr1 = "WIFI"
                }else{
                    fiturKmr1 = ""
                }
                if(cb2.isChecked){
                    fiturKmr2 = "Tahan Goyang"
                }else{
                    fiturKmr2 = ""
                }
                if(cb2.isChecked){
                    fiturKmr3 = "Sensor"
                }else{
                    fiturKmr3 = ""
                }
                queryInsertUpdateDelete("update")
                showDataKmr("")
            }
            R.id.btnHapus -> {
                queryInsertUpdateDelete("delete")
                showDataKmr("")
            }
            R.id.btnCari -> {
                showDataKmr(edNamakmr.text.toString())
            }
            R.id.imgKmr->{
                popMenu1.show()
            }
        }
   }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemMerk -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragmentLayout, fragMerk).commit()
                fragmentLayout.visibility = View.VISIBLE
                Dpd1.visibility = View.GONE
                Tpd1.visibility = View.GONE
                btnHapus.visibility = View.GONE
                btnTambah.visibility = View.GONE
                btnUbah.visibility = View.GONE
            }
            R.id.itemKamera -> {
                fragmentLayout.visibility = View.GONE
                Dpd1.visibility = View.VISIBLE
                Tpd1.visibility = View.VISIBLE
                btnHapus.visibility = View.VISIBLE
                btnTambah.visibility = View.VISIBLE
                btnUbah.visibility = View.VISIBLE
            }
            R.id.itemInfo -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragmentLayout, fragInfo).commit()
                fragmentLayout.visibility = View.VISIBLE
                Dpd1.visibility = View.GONE
                Tpd1.visibility = View.GONE
                btnHapus.visibility = View.GONE
                btnTambah.visibility = View.GONE
                btnUbah.visibility = View.GONE
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        showDataKmr("")
        getNamaMerk()
    }

    fun showDataKmr(namaKmr : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarKmr.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var kmr = HashMap<String,String>()
                    kmr.put("nama_kmt", jsonObject.getString("nama_kmr"))
                    kmr.put("type_kmr", jsonObject.getString("type_kmr"))
                    kmr.put("warna_kmr", jsonObject.getString("warna_kmr"))
                    kmr.put("fitur_kmr", jsonObject.getString("fitur_kmr"))
                    kmr.put("nama_merk", jsonObject.getString("nama_merk"))
                    kmr.put("created_at", jsonObject.getString("created_at"))
                    kmr.put("url", jsonObject.getString("url"))
                    daftarKmr.add(kmr)
                }
                kmrAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "terjadi kesalahan koneksi", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama_kmr",namaKmr)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner.setSelection(0)
            pilihMerk = daftarMerk.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihMerk = daftarMerk.get(position)
        }

    }

    val itemTouch= object : RecyclerView.OnItemTouchListener{
        override fun onTouchEvent(p0: RecyclerView, p1: MotionEvent) {}

        override fun onInterceptTouchEvent(p0: RecyclerView, p1: MotionEvent): Boolean {
            val view = p0.findChildViewUnder(p1.x,p1.y)
            val tag = p0.getChildAdapterPosition(view!!)
            val pos = daftarMerk.indexOf(daftarKmr.get(tag).get("nama_merk"))

            spinner.setSelection(pos)
            edNamakmr.setText(daftarKmr.get(tag).get("nama_kmr").toString())
            typekamera.setText(daftarKmr.get(tag).get("type_kmr").toString())
            Picasso.get().load(daftarKmr.get(tag).get("url")).into(imgKmr)

            return false
        }

        override fun onRequestDisallowInterceptTouchEvent(p0: Boolean) {}

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()) {
                imStr = data!!.data?.let { mediaHelper.getBitmapToString(it, imgKmr) }.toString()
            }else if(requestCode == mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToString2(imgKmr,fileUri)
                namaFile = mediaHelper.getMyFileName()
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    showDataKmr("")
                    Toast.makeText(this,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Operasi Gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Koneksi Gagal",Toast.LENGTH_LONG).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val namaFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
                when (mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_kmr",edNamakmr.text.toString())
                        hm.put("type_kmr",typekamera.text.toString())
                        hm.put("warna_mtr",warnaKmr1)
                        hm.put("kelengkapan_mtr",fiturKmr1+" "+fiturKmr2+" "+fiturKmr3)
                        hm.put("nama_merk",pilihMerk)
                        hm.put("created_at",txDateTime.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namaFile)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nama_kmr",edNamakmr.text.toString())
                        hm.put("type_kmr",typekamera.text.toString())
                        hm.put("warna_mtr",warnaKmr1)
                        hm.put("kelengkapan_mtr",fiturKmr1+" "+fiturKmr2+" "+fiturKmr3)
                        hm.put("nama_merk",pilihMerk)
                        hm.put("created_at",txDateTime.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namaFile)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("type_kmr",typekamera.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaMerk(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarMerk.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarMerk.add(jsonObject.getString("nama_merk"))
                }
                merkAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun RequestPermission() = runWithPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
        android.Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }

}
