package com.example.uts_1931733042

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener{

    lateinit var adapterActv : ArrayAdapter<String>
    val arrayNamaKmr = arrayOf("A6000 Kit","EOS 5Ds","D500","X-A5 Kit","PEN E-PL9","Exilim EX-TR15","LUMIX DMC-Df8","K-01 Kit","NX300M","easyshareM530")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnCari.setOnClickListener(this)
        btnOption.setOnClickListener(this)
        btnKelola.setOnClickListener(this)
        btnVideo.setOnClickListener(this)
        btnScan.setOnClickListener(this)
        btnGPS.setOnClickListener(this)
        adapterActv = ArrayAdapter(this, android.R.layout.simple_list_item_1,arrayNamaKmr)

        edCariKmr.setAdapter(adapterActv)
        edCariKmr.threshold = 2
        edCariKmr.setOnItemClickListener { parent, view, position, id ->
            val isi = adapterActv.getItem(position)
            Toast.makeText(this,isi, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClick(v: View?) {
        var popMenu = PopupMenu(this,v)
        popMenu.menuInflater.inflate(R.menu.menu_option,popMenu.menu)
        popMenu.setOnMenuItemClickListener{ item ->
            when(item.itemId){
                R.id.itemLink ->{
                    var webUri = "https://plazakamera.com"
                    var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(webUri))
                    startActivity(intentInternet)
                    Toast.makeText(this,"Membuka Link plazakamera.com", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.itemGallery ->{
                    var intentGal = Intent()
                    intentGal.setType("image/*") //filter application than can handle image
                    intentGal.setAction(Intent.ACTION_GET_CONTENT)
                    startActivity(Intent.createChooser(intentGal, "Pilih Gambar ... "))
                    Toast.makeText(this,"Membuka Gallery", Toast.LENGTH_SHORT).show()
                    true
                }
            }
            false
        }

        when(v?.id){
            R.id.btnCari -> {
                var intent = Intent(this, KmrActivity::class.java)
                intent.putExtra("cariKmr",edCariKmr.getText().toString())
                startActivity(intent)
            }
            R.id.btnKelola -> {
                var intent = Intent(this, KmrActivity::class.java)
                startActivity(intent)
            }
            R.id.btnOption -> {
                popMenu.show()
            }
            R.id.btnScan -> {
                var i = Intent(this, ScanQRCodeActivity::class.java)
                startActivity(i)
            }
            R.id.btnVideo -> {
                var i = Intent(this, VideoActivity::class.java)
                startActivity(i)
            }
            R.id.btnGPS -> {
                var i = Intent(this, GPSActivity::class.java)
                startActivity(i)
            }
        }
    }
}
