package com.example.uts_1931733042

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_scan_qr_code.*

class ScanQRCodeActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var intentIntegrator: IntentIntegrator
    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    val FIELD_FONT_SIZE = "font_size"
    val FIELD_TEXT = "teks"
    val DEF_FONT_SIZE = 12
    val DEF_TEXT = ""

    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            txScan.setTextSize(progress.toFloat())
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {
        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_qr_code)

        intentIntegrator = IntentIntegrator(this)
        btnScan.setOnClickListener(this)
        txScan.setEnabled(false)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txScan.setText(preferences.getString(FIELD_TEXT,DEF_TEXT))
        txScan.textSize = preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE).toFloat()
        sbar.progress = preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE)
        sbar.setOnSeekBarChangeListener(onSeek)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnScan -> {
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if(intentResult!=null){
            if(intentResult.contents != null){
                txScan.setText(intentResult.contents)
            }else{
                Toast.makeText(this, "Dibatalkan", Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
