package com.example.uts_1931733042

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class fragmentInfo : Fragment(), View.OnClickListener {
    lateinit var thisParent : KmrActivity
    lateinit var v : View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as KmrActivity

        v = inflater.inflate(R.layout.frag_info, container, false)

        return v
    }

    override fun onClick(v: View?) {

    }
}