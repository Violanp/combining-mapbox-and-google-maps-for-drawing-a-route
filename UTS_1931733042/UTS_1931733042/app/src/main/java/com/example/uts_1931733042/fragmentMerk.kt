package com.example.uts_1931733042

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.uts_1931733047.AdapterDataMerk
import kotlinx.android.synthetic.main.frag_merk.*
import kotlinx.android.synthetic.main.frag_merk.view.*
import kotlinx.android.synthetic.main.frag_merk.view.btnHapus
import kotlinx.android.synthetic.main.frag_merk.view.btnTambah
import kotlinx.android.synthetic.main.frag_merk.view.btnUbah
import kotlinx.android.synthetic.main.frag_merk.view.edNamaMerk
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap

class fragmentMerk : Fragment(), View.OnClickListener {
    lateinit var thisParent : KmrActivity

    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    lateinit var merkAdapter : AdapterDataMerk
    var daftarMerk = mutableListOf<HashMap<String, String>>()
    val url = "http://192.168.43.246/irvan_kamera/show_data_merk.php"
    val url2 = "http://192.168.43.246/irvan_kamera/query_upd_del_ins_merk.php"


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as KmrActivity

        v = inflater.inflate(R.layout.frag_merk, container, false)
        v.btnUbah.setOnClickListener(this)
        v.btnTambah.setOnClickListener(this)
        v.btnHapus.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)

        merkAdapter = AdapterDataMerk(daftarMerk)
        v.rvMerk.layoutManager = LinearLayoutManager(thisParent)
        v.rvMerk.adapter = merkAdapter

        v.rvMerk.addOnItemTouchListener(itemTouch)

        return v
    }


    override fun onStart() {
        super.onStart()
        showDataMerk()
    }

    fun showDataMerk(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMerk.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var merk = HashMap<String,String>()
                    merk.put("id_merk", jsonObject.getString("id_merk"))
                    merk.put("nama_merk", jsonObject.getString("nama_merk"))
                    daftarMerk.add(merk)
                }
                merkAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent, "terjadi kesalahan koneksi", Toast.LENGTH_SHORT).show()
            }
        )
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    val itemTouch= object : RecyclerView.OnItemTouchListener{
        override fun onTouchEvent(p0: RecyclerView, p1: MotionEvent) {}

        override fun onInterceptTouchEvent(p0: RecyclerView, p1: MotionEvent): Boolean {
            val view = p0.findChildViewUnder(p1.x,p1.y)
            val tag = p0.getChildAdapterPosition(view!!)

            edNamaMerk.setText(daftarMerk.get(tag).get("nama_merk").toString())
            textView7.setText(daftarMerk.get(tag).get("id_merk").toString())

            return false
        }

        override fun onRequestDisallowInterceptTouchEvent(p0: Boolean) {}

    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    showDataMerk()
                    Toast.makeText(thisParent,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(thisParent,"Operasi Gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Koneksi Gagal",Toast.LENGTH_LONG).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when (mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_merk",edNamaMerk.text.toString())
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("id_merk",textView7.text.toString())
                        hm.put("nama_merk",edNamaMerk.text.toString())
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_merk",textView7.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambah ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUbah ->{
                queryInsertUpdateDelete("update")
                showDataMerk()
            }
            R.id.btnHapus ->{
                queryInsertUpdateDelete("delete")
                showDataMerk()
            }
        }
    }

}