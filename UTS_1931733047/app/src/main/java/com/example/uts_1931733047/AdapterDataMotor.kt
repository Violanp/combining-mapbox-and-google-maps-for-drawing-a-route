package com.example.uts_1931733047

import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataMotor (val dataMtr : List<HashMap<String,String>>) : RecyclerView.Adapter<AdapterDataMotor.HolderDataMotor>() {
    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterDataMotor.HolderDataMotor {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_motor, p0, false)
        return HolderDataMotor(v)
    }

    override fun getItemCount(): Int {
        return dataMtr.size
    }

    override fun onBindViewHolder(p0: AdapterDataMotor.HolderDataMotor, p1: Int) {
        val data = dataMtr.get(p1)
        p0.txNamaMtr.setText(data.get("nama_mtr"))
        p0.txPlatMtr.setText(data.get("plat_mtr"))
        p0.txWarnaMtr.setText(data.get("warna_mtr"))
        p0.txKelengkapanMtr.setText(data.get("kelengkapan_mtr"))
        p0.txMerkMtr.setText(data.get("merk_motor"))
        p0.txTanggal.setText(data.get("created_at"))

        if(p1.rem(2) == 0)p0.cLayout.setBackgroundColor(Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.img)
    }

    class HolderDataMotor (v: View):RecyclerView.ViewHolder(v) {
        val txNamaMtr = v.findViewById<TextView>(R.id.txNamaMtr)
        val txPlatMtr = v.findViewById<TextView>(R.id.txPlatMtr)
        val txWarnaMtr = v.findViewById<TextView>(R.id.txWarnaMtr)
        val txKelengkapanMtr = v.findViewById<TextView>(R.id.txKelengkapanMtr)
        val txMerkMtr = v.findViewById<TextView>(R.id.txMerkMtr)
        val txTanggal = v.findViewById<TextView>(R.id.txTanggal)
        val img = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.layoutItemMotor)

    }
}