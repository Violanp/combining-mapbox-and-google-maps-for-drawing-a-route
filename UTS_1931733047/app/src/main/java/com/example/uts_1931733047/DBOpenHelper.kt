package com.example.uts_1931733047

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context):SQLiteOpenHelper(context,DB_Name, null,DB_Ver){
    override fun onCreate(db: SQLiteDatabase?) {
        val tMtr = "create table mtr(id_mtr integer primary key autoincrement, nama_mtr text not null, plat_mtr text not null, warna_mtr text not null, kelengkapan_mtr text not null, id_merk int not null, created_at text not null)"
        val tMerk = "create table merk(id_merk integer primary key autoincrement, nama_merk text not null)"
        val insMtr = "insert into mtr(nama_mtr,plat_mtr,warna_mtr,kelengkapan_mtr,id_merk,created_at) values" +
                "('Mio M3 125','AG100U','Biru','BPKB STNK','1','31-12-2019 18:00')," +
                "('Beat F1 150','AG100R','Merah','BPKB STNK','2','31-12-2019 18:00')," +
                "('Ninja RR','AG553W','Hitam','BPKB STNK','3','31-12-2019 18:00')," +
                "('Fiz R','AG700T','Merah','BPKB','4','01-01-2020 13:00')," +
                "('TVS Neo','AG656O','Biru','STNK','5','01-01-2020 13:00')," +
                "('Iron 883','B300Y','Hitam','BPKB STNK','6','10-02-2020 16:00')," +
                "('Ducati Diavel','B276I','Merah','BPKB STNK','7','10-02-2020 16:00')," +
                "('Vespa Sprint 150','B190P','Putih','BPKB STNK','8','10-02-2020 16:00')," +
                "('KTM Duke 200','B980L','Putih','BPKB STNK','9','16-03-2020 7:00')," +
                "('Viar ATV','B789K','Biru','BPKB STNK','10','16-03-2020 7:00')"
        val insMerk = "insert into merk(nama_merk) values('Yamaha'),('Honda'),('Kawasaki'),('Suzuki'),('TVS Motor'),('Harley Davidson'),('Ducati'),('Vespa'),('KTM'),('VIAR')"
        db?.execSQL(tMtr)
        db?.execSQL(tMerk)
        db?.execSQL(insMtr)
        db?.execSQL(insMerk)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    companion object {
        val DB_Name = "motor"
        val DB_Ver = 1
    }
}