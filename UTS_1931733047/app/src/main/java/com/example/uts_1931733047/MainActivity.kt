package com.example.uts_1931733047

import android.app.Activity
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener{

    lateinit var adapterActv : ArrayAdapter<String>
    val arrayNamaMtr = arrayOf("Mio M3 125","Beat F1 150","Ninja RR","Fiz R","TVS Neo","Iron 883","Ducati Diave","Vespa Sprint 150","KTM Duke 200","Viar ATV")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnCari.setOnClickListener(this)
        btnOption.setOnClickListener(this)
        btnKelola.setOnClickListener(this)
        btnQrCode.setOnClickListener(this)
        btnVideo.setOnClickListener(this)
        btnShareLoc.setOnClickListener(this)
        adapterActv = ArrayAdapter(this, android.R.layout.simple_list_item_1,arrayNamaMtr)

        edCariMtr.setAdapter(adapterActv)
        edCariMtr.threshold = 2
        edCariMtr.setOnItemClickListener { parent, view, position, id ->
            val isi = adapterActv.getItem(position)
            Toast.makeText(this,isi, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClick(v: View?) {
        var popMenu = PopupMenu(this,v)
        popMenu.menuInflater.inflate(R.menu.menu_option,popMenu.menu)
        popMenu.setOnMenuItemClickListener{ item ->
            when(item.itemId){
                R.id.itemLink ->{
                    var webUri = "https://momotor.id"
                    var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(webUri))
                    startActivity(intentInternet)
                    Toast.makeText(this,"Membuka Link Momotor.id", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.itemGallery ->{
                    var intentGal = Intent()
                    intentGal.setType("image/*") //filter application than can handle image
                    intentGal.setAction(Intent.ACTION_GET_CONTENT)
                    startActivity(Intent.createChooser(intentGal, "Pilih Gambar ... "))
                    Toast.makeText(this,"Membuka Gallery", Toast.LENGTH_SHORT).show()
                    true
                }
            }
            false
        }

        when(v?.id){
            R.id.btnCari -> {
                var i = Intent(this, MotorActivity::class.java)
                i.putExtra("cariMtr",edCariMtr.getText().toString())
                startActivity(i)
            }
            R.id.btnKelola -> {
                var i = Intent(this, MotorActivity::class.java)
                startActivity(i)
            }
            R.id.btnOption -> {
                popMenu.show()
            }
            R.id.btnQrCode -> {
                var i = Intent(this, QRCodeActivity::class.java)
                startActivity(i)
            }
            R.id.btnVideo -> {
                var i = Intent(this, VideoActivity::class.java)
                startActivity(i)
            }
            R.id.btnShareLoc -> {
                var i = Intent(this, ShareLocActivity::class.java)
                startActivity(i)
            }
        }
    }
}
