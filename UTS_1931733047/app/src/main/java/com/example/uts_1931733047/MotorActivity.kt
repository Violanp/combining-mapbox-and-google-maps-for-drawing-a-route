package com.example.uts_1931733047

import android.app.*
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_motor.*
import kotlinx.android.synthetic.main.item_motor.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class MotorActivity : AppCompatActivity(), View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {

    var tahun = 0
    var bulan = 0
    var hari = 0
    var jam = 0
    var menit = 0

    lateinit var mediaHelper : MediaHelper
    lateinit var mtrAdapter : AdapterDataMotor
    lateinit var merkAdapter : ArrayAdapter<String>
    var daftarMtr = mutableListOf<HashMap<String, String>>()
    var daftarMerk = mutableListOf<String>()
    val url = "http://192.168.43.246/fuu_motor/show_data.php"
    val url2 = "http://192.168.43.246/fuu_motor/get_nama_merk.php"
    val url3 = "http://192.168.43.246/fuu_motor/query_upd_del_ins.php"
    var imStr = ""
    var pilihMerk = ""
    var namaFile = ""
    var fileUri = Uri.parse("")

    lateinit var dialog : AlertDialog.Builder
    lateinit var fragMerk : fragmentMerk
    lateinit var fragAbout : fragmentAbout
    lateinit var ft: FragmentTransaction
    var warnaMtr1 = ""
    var kelengkapanMtr1 = ""
    var kelengkapanMtr2 = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_motor)
        val cal : Calendar = Calendar.getInstance()
        bulan = cal.get(Calendar.MONTH)
        hari = cal.get(Calendar.DAY_OF_MONTH)
        tahun = cal.get(Calendar.YEAR)
        jam = cal.get(Calendar.HOUR)
        menit = cal.get(Calendar.MINUTE)

        edJam.setText("$jam:$menit")
        edTgl.setText("$hari-${bulan+1}-$tahun")

        edJam.setOnClickListener(this)
        edTgl.setOnClickListener(this)

        fragMerk = fragmentMerk()
        fragAbout = fragmentAbout()

        bnv1.setOnNavigationItemSelectedListener(this)
        edNamaMtr.setText(getIntent().getStringExtra("cariMtr"));
        dialog = AlertDialog.Builder(this)
        btnTambah.setOnClickListener(this)
        btnUbah.setOnClickListener(this)
        btnHapus.setOnClickListener(this)
        btnCari.setOnClickListener(this)

        rg1.setOnCheckedChangeListener{ group, checkedId ->
            when(checkedId){
                R.id.rb1 -> warnaMtr1 = "Hitam"
                R.id.rb2 -> warnaMtr1 = "Putih"
                R.id.rb3 -> warnaMtr1 = "Biru"
                R.id.rb4 -> warnaMtr1 = "Merah"
            }
        }

        mtrAdapter = AdapterDataMotor(daftarMtr)
        rvMtr.layoutManager = LinearLayoutManager(this)
        rvMtr.adapter = mtrAdapter
        mediaHelper = MediaHelper(this)

        merkAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarMerk)
        spinner.adapter = merkAdapter
        spinner.onItemSelectedListener = itemSelected
        imgMtr.setOnClickListener(this)
        rvMtr.addOnItemTouchListener(itemTouch)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception){
            e.printStackTrace()
        }
    }

    var timeChangeDialog = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
        edJam.setText("$hourOfDay:$minute")
        jam = hourOfDay
        menit = minute
        Toast.makeText(this,"Jam Berhasil Diinputkan", Toast.LENGTH_SHORT).show()
    }

    var dateChangeDialog = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
        edTgl.setText("$dayOfMonth-${month+1}-$year")
        tahun = year
        bulan = month
        hari = dayOfMonth
        Toast.makeText(this,"Tanggal Berhasil Diinputkan", Toast.LENGTH_SHORT).show()
    }

    override fun onCreateDialog(id: Int): Dialog {
        when(id){
            10 -> return TimePickerDialog(this, timeChangeDialog,jam,menit, true)
            20 -> return DatePickerDialog(this, dateChangeDialog,tahun,bulan,hari)
        }
        return super.onCreateDialog(id)
    }

    override fun onClick(v: View?) {
        var popMenu1 = PopupMenu(this,v)
        popMenu1.menuInflater.inflate(R.menu.menu_camera,popMenu1.menu)
        popMenu1.setOnMenuItemClickListener{ item ->
            when(item.itemId){
                R.id.itemAmbilFoto ->{
                    RequestPermission()
                }
                R.id.itemPilihGallery ->{
                    val intent = Intent()
                    intent.setType("image/*")
                    intent.setAction(Intent.ACTION_GET_CONTENT)
                    startActivityForResult(intent, mediaHelper.getRcGallery())
                }
            }
            false
        }

        when(v?.id){
            R.id.edTgl -> {
                showDialog(20)
                Toast.makeText(this,"Input Tanggal", Toast.LENGTH_SHORT).show()
            }
            R.id.edJam -> {
                showDialog(10)
                Toast.makeText(this,"Input Jam", Toast.LENGTH_SHORT).show()
            }
            R.id.btnTambah -> {
                if(cb1.isChecked) kelengkapanMtr1 = "BPKB"
                if(cb2.isChecked) kelengkapanMtr2 = "STNK"
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUbah -> {
                if(cb1.isChecked){
                    kelengkapanMtr1 = "BPKB"
                }else{
                    kelengkapanMtr1 = ""
                }
                if(cb2.isChecked){
                    kelengkapanMtr2 = "STNK"
                }else{
                    kelengkapanMtr2 = ""
                }
                queryInsertUpdateDelete("update")
                showDataMtr("")
            }
            R.id.btnHapus -> {
                queryInsertUpdateDelete("delete")
                showDataMtr("")
            }
            R.id.btnCari -> {
                showDataMtr(edNamaMtr.text.toString())
            }
            R.id.imgMtr->{
                popMenu1.show()
            }
        }
   }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemMerk -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragmentLayout, fragMerk).commit()
                fragmentLayout.visibility = View.VISIBLE
                btnCari.visibility = View.GONE
                btnHapus.visibility = View.GONE
                btnTambah.visibility = View.GONE
                btnUbah.visibility = View.GONE
            }
            R.id.itemMotor -> {
                fragmentLayout.visibility = View.GONE
                btnCari.visibility = View.VISIBLE
                btnHapus.visibility = View.VISIBLE
                btnTambah.visibility = View.VISIBLE
                btnUbah.visibility = View.VISIBLE
            }
            R.id.itemAbout -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragmentLayout, fragAbout).commit()
                fragmentLayout.visibility = View.VISIBLE
                btnHapus.visibility = View.GONE
                btnTambah.visibility = View.GONE
                btnUbah.visibility = View.GONE
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        showDataMtr("")
        getNamaMerk()
    }

    fun showDataMtr(namaMtr : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMtr.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mtr = HashMap<String,String>()
                    mtr.put("nama_mtr", jsonObject.getString("nama_mtr"))
                    mtr.put("plat_mtr", jsonObject.getString("plat_mtr"))
                    mtr.put("warna_mtr", jsonObject.getString("warna_mtr"))
                    mtr.put("kelengkapan_mtr", jsonObject.getString("kelengkapan_mtr"))
                    mtr.put("nama_merk", jsonObject.getString("nama_merk"))
                    mtr.put("created_at", jsonObject.getString("created_at"))
                    mtr.put("url", jsonObject.getString("url"))
                    daftarMtr.add(mtr)
                }
                mtrAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "terjadi kesalahan koneksi", Toast.LENGTH_SHORT).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama_mtr",namaMtr)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner.setSelection(0)
            pilihMerk = daftarMerk.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihMerk = daftarMerk.get(position)
        }

    }

    val itemTouch= object : RecyclerView.OnItemTouchListener{
        override fun onTouchEvent(p0: RecyclerView, p1: MotionEvent) {}

        override fun onInterceptTouchEvent(p0: RecyclerView, p1: MotionEvent): Boolean {
            val view = p0.findChildViewUnder(p1.x,p1.y)
            val tag = p0.getChildAdapterPosition(view!!)
            val pos = daftarMerk.indexOf(daftarMtr.get(tag).get("nama_merk"))

            spinner.setSelection(pos)
            edNamaMtr.setText(daftarMtr.get(tag).get("nama_mtr").toString())
            edPlatMtr.setText(daftarMtr.get(tag).get("plat_mtr").toString())
            Picasso.get().load(daftarMtr.get(tag).get("url")).into(imgMtr)

            return false
        }

        override fun onRequestDisallowInterceptTouchEvent(p0: Boolean) {}

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()) {
                imStr = data!!.data?.let { mediaHelper.getBitmapToString(it, imgMtr) }.toString()
            }else if(requestCode == mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToString2(imgMtr,fileUri)
                namaFile = mediaHelper.getMyFileName()
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    showDataMtr("")
                    Toast.makeText(this,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Operasi Gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Koneksi Gagal",Toast.LENGTH_LONG).show()
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val namaFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
                when (mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_mtr",edNamaMtr.text.toString())
                        hm.put("plat_mtr",edPlatMtr.text.toString())
                        hm.put("warna_mtr",warnaMtr1)
                        hm.put("kelengkapan_mtr",kelengkapanMtr1+" "+kelengkapanMtr2)
                        hm.put("nama_merk",pilihMerk)
                        hm.put("created_at",edTgl.text.toString()+" "+edJam.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namaFile)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nama_mtr",edNamaMtr.text.toString())
                        hm.put("plat_mtr",edPlatMtr.text.toString())
                        hm.put("warna_mtr",warnaMtr1)
                        hm.put("kelengkapan_mtr",kelengkapanMtr1+" "+kelengkapanMtr2)
                        hm.put("nama_merk",pilihMerk)
                        hm.put("created_at",edTgl.text.toString()+" "+edJam.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namaFile)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("plat_mtr",edPlatMtr.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaMerk(){
        val request = StringRequest(Request.Method.POST,url2,
            Response.Listener { response ->
                daftarMerk.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarMerk.add(jsonObject.getString("nama_merk"))
                }
                merkAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun RequestPermission() = runWithPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
        android.Manifest.permission.CAMERA){
            fileUri = mediaHelper.getOutputMediaFileUri()
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
            startActivityForResult(intent,mediaHelper.getRcCamera())
    }

}
