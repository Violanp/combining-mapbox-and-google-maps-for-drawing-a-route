package com.example.uts_1931733047

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class fragmentAbout : Fragment(), View.OnClickListener {

    lateinit var thisParent : MotorActivity
    lateinit var v : View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MotorActivity

        v = inflater.inflate(R.layout.frag_about, container, false)

        return v
    }
    override fun onClick(v: View?) {

    }
}